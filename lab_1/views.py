from django.shortcuts import render

def home(request):
    return render(request, 'story01.html')
def profile(request):
    return render(request, 'profile.html')
def aboutme(request):
    return render(request, 'aboutme.html')
def contact(request):
    return render(request, 'Contact.html')
def register(request):
    return render(request,'Register.html')